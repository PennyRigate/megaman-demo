extends Node2D

var speed = 240
onready var player = get_parent().get_node("Player")

#determine direction
func _ready():
	scale.x = player.graphics.scale.x
	player.lemons_onscreen += 1
	
	
func _physics_process(delta):
	position.x += (speed * scale.x) * delta


func _on_VisibilityNotifier2D_screen_exited():
	player.lemons_onscreen -= 1
	queue_free()
