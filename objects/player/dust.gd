extends AnimatedSprite

onready var player = get_parent().get_node("Player")

func _ready():
	scale.x = player.graphics.scale.x
	set_playing(true)

func _on_AnimatedSprite_animation_finished():
	queue_free()
