extends KinematicBody2D

##States
enum State {IDLE,WALK,JUMP,FALL,STUNNED,SLIDE}
var current_state = State.IDLE
##Onreadys
onready var anims = $Graphics/AnimationPlayer
onready var graphics = $Graphics
onready var sprite = $Graphics/Sprite
onready var shoot_sprite = $Graphics/ShootSprite
onready var shoot_return = $Graphics/ShootReturn
onready var slide_dist = $SlideDist
onready var map = get_parent()
onready var audio = $AudioStreamPlayer
##Physics
var velocity = Vector2.ZERO
var axis = Vector2.ZERO
var walk_speed = 70
var gravity = 600
var slide_speed = 180
var jump_pressure = 0
var max_jump_pressure = 8
var jump_force = 330
var dust_pos = Vector2(-9,9)
##Weapons
var shoot_return_anim = 0
var lemons_onscreen = 0
onready var shoot_pos = 20
##Sounds
var snd_land = preload("res://audio/megaman/land.wav")
var snd_shoot = preload("res://audio/megaman/shoot.wav")
##Preload
var pre_lemon = preload("res://objects/player/lemon.tscn")
var pre_dust = preload("res://objects/player/dust.tscn")
var pre_telly = preload("res://objects/enemy/telly.tscn")


func _physics_process(delta):
	axis = Vector2(Input.get_axis("ui_left","ui_right"),Input.get_axis("ui_up","ui_down"))
	#if axis.x != 0: velocity.x = walk_speed * axis.x
	match current_state:
		State.IDLE:
			_process_idle()
			continue
		State.WALK:
			_process_walk()
			continue
		State.IDLE, State.WALK:
			_process_idle_walk()
		State.JUMP:
			_process_jump()
			continue
		State.JUMP, State.FALL:
			_process_fall()
			continue
		State.SLIDE:
			_process_slide()
		
	#Gravity
	velocity.y += gravity * delta
	#Apply velocity
	move_and_slide(velocity,Vector2.UP)
	#Shoot
	if Input.is_action_just_pressed("shoot") && lemons_onscreen < 3 && current_state != State.SLIDE:
		Game.play_sound(snd_shoot,audio)
		shoot()
	#Spawn enemy
	if Input.is_action_just_pressed("spawn"):
		Game.instance_node(pre_telly,rand_range(16,260),-8,map)
	

func _process_idle():
	if anims.current_animation != "idle": anims.play("idle")
	#Stop
	velocity.x = 0
	#Goto Walk
	if axis.x != 0: current_state = State.WALK

func _process_walk():
	if anims.current_animation != "walk": anims.play("walk")
	#Move
	move(walk_speed,0,true)
	#Goto Idle
	if axis.x == 0: current_state = State.IDLE

func _process_idle_walk():
	#Goto Fall
	if !is_on_floor(): current_state = State.FALL
	#Goto Jump
	check_jump()
	#Goto Slide
	if Input.is_action_pressed("ui_down") && Input.is_action_just_pressed("jump"):
		slide_dist.start()
		Game.instance_node(pre_dust,position.x + (dust_pos.x * graphics.scale.x),position.y + dust_pos.y,map)
		current_state = State.SLIDE
		return

func _process_jump():
	jump_pressure += 1
	#Pressure sensitive jump
	if jump_pressure == 10 or Input.is_action_just_released("jump"):
		velocity.y = -jump_force / 4
		#velocity.y = 0
		current_state = State.FALL

func _process_fall():
	anims.play("jump")
	move(walk_speed,0,true)
	#Return to idle
	if is_on_floor():
		if !audio.is_playing(): Game.play_sound(snd_land,audio)
		current_state = State.IDLE
		return

func _process_slide():
	anims.play("slide")
	#Slide
	velocity.x = slide_speed * graphics.scale.x
	#Cancel with jump
	check_jump()
	#Stop on wall
	if is_on_wall():
		current_state = State.IDLE
		return

func move(hsp,vsp,flip:bool):
	velocity.x = hsp * axis.x
	#Flip
	if flip: if sign(axis.x) != 0: graphics.scale.x = axis.x
	
func check_jump() -> void:
	if Input.is_action_just_pressed("jump") && !Input.is_action_pressed("ui_down"):
		# Jump
		jump_pressure = 0
		current_state = State.JUMP
		anims.play("jump")
		velocity.y = -jump_force

func shoot():
	shoot_return.start()
	sprite.visible = false
	shoot_sprite.visible = true
	Game.instance_node(pre_lemon,position.x + (shoot_pos * graphics.scale.x),position.y,map)

func _on_ShootReturn_timeout():
	sprite.visible = true
	shoot_sprite.visible = false


func _on_SlideDist_timeout():
	if is_on_floor(): current_state = State.IDLE
	return
