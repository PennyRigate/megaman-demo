extends Node

var resolution = Vector2(272,240)

#Onready
onready var enemy_death_sound = $EnemyDeathSound

func instance_node(node:PackedScene,x:float,y:float,parent):
		var Instance = node.instance()
		Instance.global_position = Vector2(x,y)
		parent.add_child(Instance)

func play_sound(snd,player):
	player.set_stream(snd)
	player._set_playing(true)
