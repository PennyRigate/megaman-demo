extends Node2D

onready var player = get_parent().get_node("Player")
var speed = 50
var pre_explosion = preload("res://objects/enemy/enemy_explosion.tscn")

func _physics_process(delta):
	position = position.move_toward(player.position,speed * delta)


func _on_Area2D_area_entered(area):
	if area.is_in_group("lemon"):
		Game.instance_node(pre_explosion,position.x,position.y,get_parent())
		Game.enemy_death_sound.play()
		area.get_parent().queue_free()
		queue_free()
