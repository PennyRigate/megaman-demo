Megaman demo by Penny Rigate 2022 made in Godot

Coded in about 10 hours

I was intending on prototyping a Megaman clone last night but I got obsessed with trying to port Megaman's physics into godot,
so i opened up FCEUX and started observing how megaman ticks, the math i did to replicate the physics was very approximate and 
the code really needs to be cleaned up but for something done in an entire sleep deprived AM, I'm very proud of the results.

-Controls-
X (Keyboard), A (Xbox), Cross (PS): Jump
Z (Keyboard), X (Xbox), Square (PS): Shoot
You can press down and jump to slide
H (Keyboard), Y (Xbox), Triangle (PS): Hide tip menu
S (Keyboard), B (Xbox), Circle (PS): Spawn enemy