extends Control

var multiple = 1
onready var viewport = $ViewportContainer
onready var border = $TextureRect
#	SCALE MODE: INTEGER, FIT, FILL
export var scale_mode = 0
#	INTEGER SCALE SETTINGS
export var overscale = 0
onready var root = get_tree().root
#	RESOLUTION
onready var res = Game.resolution
onready var half_res = Vector2(res.x/2,res.y/2)

func _ready():
	#SCREEN RESIZE SIGNAL
	get_tree().connect("screen_resized", self, "_on_screen_resized")
	root.set_attach_to_screen_rect(root.get_visible_rect())
	_on_screen_resized()

func _on_screen_resized():
#	VARS
	var window_size = OS.get_window_size()
#	INTEGER SCALE
	if scale_mode == 0:
		#	CENTER THE VIEWPORT
		viewport.rect_position.x = (window_size.x / 2) - half_res.x
		viewport.rect_position.y = (window_size.y / 2) - half_res.y
		#	DETERMINE WHAT THE HIGHEST INTEGER MULTIPLE IS
		multiple = window_size / res
		multiple = Vector2(floor(multiple.x),floor(multiple.y))
		#	SET THE HIGHEST SCALE AXIS TO THE LOWEST TO STAY SQUARE
		if multiple.x < multiple.y: multiple.y = multiple.x
		if multiple.x > multiple.y: multiple.x = multiple.y
		#	SCALE THE VIEWPORT (IF OVERSCALE IS ON, SCALE IT BY 1 EXTRA)
		viewport.rect_scale = multiple + Vector2(overscale,overscale)
		#viewport.rect_scale = Vector2(2,2)
#	SCALE TO FIT
	if scale_mode == 1:
		get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT,SceneTree.STRETCH_ASPECT_KEEP,res,1)
#	BORDER
	border.rect_size = window_size

